package qqft.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataRest.class);


    @GetMapping("/healthcheck")
    public ResponseEntity<String> healthcheck(){
        return new ResponseEntity<>("Alives!",HttpStatus.OK);
    }

}
